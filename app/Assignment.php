<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id',
        'assignment_id',
        'assignment_type',
    ];


}
