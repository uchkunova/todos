<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the users from the group.
     */
    public function users() {
        return $this->belongsToMany('App\User');
    }

    /**
     * Get all tasks that are assigned to the group
     */
    public function assignedTasks()
    {
        return $this->morphToMany('App\Task', 'assignment');
    }


}
