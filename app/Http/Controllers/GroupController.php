<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\StoreGroup;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the groups
     *
     * @return View
     */
    public function index() : View
    {
        return view('groups.index', ['groups' => Auth::user()->groups()->get()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id) : View
    {
        $group = Group::with('users', 'assignedTasks')->find($id);
        $allUsers = User::all();

        return view('groups.show', [
            'group' => $group,
            'allUsers' => $allUsers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create() : View
    {
        return view('groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreGroup  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroup $request) : Response
    {
        $group = Group::create($request->all());
        $group->users()->attach(Auth::user()->id);

        return response($group);
    }

    /**
     * Add participant to group
     *
     ** @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addParticipant(Request $request) : Response
    {
        $data = explode('/', $request->path());
        $userId = $data[3];

        try {
            User::findOrFail($userId);
        } catch (NotFoundHttpException $e) {
            return response(['error' => $e->getMessage()]);
        }

        $groupId = $data[1];
        Group::find($groupId)->users()->attach($userId);

        return response(['message' => 'User was added']);
    }

    /**
     * Remove participant to group
     *
     ** @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function removeParticipant(Request $request) : Response
    {
        $data = explode('/', $request->path());
        $userId = $data[3];

        try {
            User::findOrFail($userId);
        } catch (NotFoundHttpException $e) {
            return response(['error' => $e->getMessage()]);
        }

        $groupId = $data[1];
        Group::find($groupId)->users()->detach($userId);

        return response(['message' => 'User was removed']);
    }
}
