<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTask;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('category')->where(['owner_id' => Auth::user()->id])->orderBy('created_at', 'desc')->get();

        return view('tasks.index', [
            'tasks' => $tasks,
            'assignmentTypes' => Task::getAssignmentTypes(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTask $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTask $request)
    {
        DB::beginTransaction();

        $task = Task::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'owner_id' => Auth::user()->id,
            'category_id' => $request->input('category_id'),
            'assigned_time' => $request->input('assigned_time'),
        ]);

        if ($request->input('assignment_type') != null) {
            $task->assign(
                $request->input('assignment_id'),
                $request->input('assignment_type')
            );
        }

        DB::commit();

        return response($task);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::with(['category', 'owner', 'assignedUsers', 'assignedGroups'])->find($id);

        return view('tasks.show', ['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreTask  $request
     * @param  Task  $task
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTask $request, Task $task)
    {
        $task->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'assigned_time' => $request->input('assigned_time'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
