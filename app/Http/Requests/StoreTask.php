<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        //TODO: authorization
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'description' => 'max:500',
            'category_id' => 'exists:categories,id',
            'owner_id' => 'exists:users,id',
            'assignment_type' => 'in:App\User,App\Group',
            'assignment_id' => 'integer|required_with:assignment_type',
            'assigned_time' => 'date',
        ];
    }
}
