<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends BaseModel
{
    const ASSIGNMENT_TYPE_USER = 'App\User';
    const ASSIGNMENT_TYPE_GROUP = 'App\Group';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'owner_id',
        'category_id',
        'assignment_id',
        'assigned_time',
    ];

    public $nullableFields = [
        'category_id',
        'description',
        'assignment_id',
        'assigned_time',
    ];

    /**
     * Get the category.
     */
    public function category() {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the user.
     */
    public function owner() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get all users that the task is assigned to
     */
    public function assignedUsers()
    {
        return $this->morphedByMany('App\User', 'assignment');
    }

    /**
     * Get all groups that the task is assigned to
     */
    public function assignedGroups()
    {
        return $this->morphedByMany('App\Group', 'assignment');
    }

    /**
     * @param $userId
     */
    public function assignToUser($userId)
    {
        Assignment::create([
            'task_id' => $this->id,
            'assignment_id' => $userId,
            'assignment_type' => User::class,
        ]);
    }

    /**
     * @param $groupId
     */
    public function assignToGroup($groupId)
    {
        Assignment::create([
            'task_id' => $this->id,
            'assignment_id' => $groupId,
            'assignment_type' =>Group::class,
        ]);
    }

    /**
     * @param $assignment_id
     * @param $assignment_type
     * @throws \Exception
     */
    public function assign($assignment_id, $assignment_type)
    {
        switch ($assignment_type) {
            case self::ASSIGNMENT_TYPE_USER:
                $this->assignToUser($assignment_id);
                break;

            case self::ASSIGNMENT_TYPE_GROUP:
                $this->assignToGroup($assignment_id);
                break;
            default:
                throw new \Exception('Invalid assignment type passed');
        }
    }

    /**
     * @return string
     */
    public static function getAssignmentTypes() {
        return json_encode([
            'user' => self::ASSIGNMENT_TYPE_USER,
            'group' => self::ASSIGNMENT_TYPE_GROUP
        ]);
    }

}
