<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the tasks of the user.
     */
    public function tasks() {
        return $this->hasMany('App\Task');
    }

    /**
     * Get the group.
     */
    public function groups() {
        return $this->belongsToMany('App\Group');
    }

    /**
     * Get all tasks that are assigned to the user
     */
    public function assignedTasks()
    {
        return $this->morphToMany('App\Task', 'assignment');
    }

}
