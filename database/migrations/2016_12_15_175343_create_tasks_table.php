<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();

            $table->integer('owner_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            //$table->integer('assigned_to')->unsigned()->nullable();
            $table->timestamp('assigned_time')->nullable();

            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
            //$table->foreign('assigned_to')->references('id')->on('assignments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
