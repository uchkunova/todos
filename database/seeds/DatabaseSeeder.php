<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GroupsTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        for($i=1; $i<=3; $i++) {
            $user = App\User::find($i);
            $user->groups()->attach(1);
        }

        $this->call(CategoriesTableSeeder::class);
        $this->call(TasksTableSeeder::class);
    }
}
