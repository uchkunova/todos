<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@test.test',
            'password' => bcrypt('111111'),
        ]);

        $users = factory(App\User::class, 10)->create();

        foreach($users as $user) {
            $user->groups()->attach(rand(1,3));
        }

    }
}
