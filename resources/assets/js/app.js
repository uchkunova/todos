
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

import Task from './components/shared/Task.vue';
import UserCard from './components/shared/UserCard.vue';

import TasksIndex from './components/pages/TasksIndex.vue';
import TasksShow from './components/pages/TasksShow.vue';
import GroupsIndex from './components/pages/GroupsIndex.vue';
import GroupsShow from './components/pages/GroupsShow.vue';

Vue.prototype.$http = axios;

const app = new Vue({
    el: '#app',

    components: {
        Task,
        UserCard,
        TasksIndex,
        TasksShow,
        GroupsIndex,
        GroupsShow
    }

});






