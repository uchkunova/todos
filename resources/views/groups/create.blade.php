@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="form-group">
            <a href="{{ url('/groups') }}">Back</a>
        </div>

        <div class="form-group">
            <form class="form-horizontal" method="POST" action="{{ url('/groups') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Name</label>
                    <div class="col-md-6">
                        <input id="name"
                               type="text"
                               name="name"
                               class="form-control"
                        >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <input type="submit" value="Go" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection













