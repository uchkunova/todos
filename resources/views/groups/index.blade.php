@extends('layouts.app')

@section('content')
    <groups-index :groups="{{ $groups }}" inline-template>
        <div class="container">
            <div class="col-md-9 col-md-offset-3">
                <div class="form-group row">
                    <h1>My Groups</h1>
                </div>

                <div class="form-group row">
                    <button class="btn btn-primary pull-right" @click="showNewGroupForm=!showNewGroupForm">Add New Group</button>
                </div>

                <div class="form-group row" v-show="showNewGroupForm==true">
                    <new-group @applied="addGroup"></new-group>
                </div>

                <div class="form-group row">
                    <ul class="list-group"v-for="group in groups">
                        <li class="list-group-item">
                            <a :href="'groups/'+group.id">@{{group.name}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </groups-index>
@endsection













