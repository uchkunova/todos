@extends('layouts.app')

@section('content')
    <groups-show :group="{{ $group }}" :users="{{ $allUsers }}" inline-template>
        <div class="container">
            <div class="col-md-9 col-md-offset-3">
                <div class="form-group row">
                    <h1> {{ $group->name }}</h1>

                    <div>
                        <div class="form-group">
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" @click="showingTasks">Tasks</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" @click="showingParticipants">Participants</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" @click="showingAddUsers">Add/Remove Users</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" v-show="showTasks" style="display: none">
                            <h3>Tasks</h3>

                            <div v-for="task in group.assigned_tasks">
                                <task :task="task" :url="'{{asset('/tasks/')}}/'+task.id"></task>
                            </div>
                        </div>

                        <div class="form-group" v-show="showParticipants" style="display: none">
                            <h3>Participants</h3>

                            <div v-for="user in group.users">
                                <user-card :user="user"></user-card>
                            </div>
                        </div>

                        <div class="form-group" v-show="showAddUsers" style="display: none">
                            <h3>Add Users</h3>

                            <ul class="list-group" v-for="user in users">
                                <li class="list-group-item">
                                    <div>@{{ user.name }}
                                        <div class="pull-right" v-show="ids.indexOf(user.id)==-1">
                                            <button class="btn btn-primary btn-sm" @click="addUserToGroup(user.id)"
                                            >Add</button>
                                        </div>
                                        <div class="pull-right" v-show="ids.indexOf(user.id)!=-1">
                                            <button class="btn btn-danger btn-sm" @click="removeUserFromGroup(user.id)"
                                            >Remove</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </groups-show>
@endsection











