@extends('layouts.app')

@section('content')
    <tasks-index :tasks="{{ $tasks }}" inline-template>
        <div class="container">
            <div class="col-md-9 col-md-offset-3">
                <div class="form-group row">
                    <button class="btn btn-lg btn-default" @click="showNewTaskForm=!showNewTaskForm">Add New Task</button>

                    <div class="form-group row" v-show="showNewTaskForm==true">
                        <new-task @applied="addTask"></new-task>
                    </div>
                </div>
                <div class="form-group row" v-for="task in tasks">
                    <task :task="task" :url="'{{asset('/tasks/')}}/'+task.id"></task>
                </div>
            </div>
        </div>
    </tasks-index>
@endsection













