<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('groups', 'GroupController@index');
Route::get('/groups/create', 'GroupController@create');
Route::post('/groups', 'GroupController@store');
Route::get('groups/{group}', 'GroupController@show');

Route::get('/tasks', 'TaskController@index');
Route::get('/tasks/create', 'TaskController@create');
Route::post('/tasks', 'TaskController@store');
Route::get('/tasks/{task}', 'TaskController@show');
Route::get('/tasks/{task}/edit', 'TaskController@edit');
Route::patch('/tasks/{task}', 'TaskController@update');
Route::delete('/tasks/{task}', 'TaskController@destroy');

Route::get('api/categories', function () {
    return json_encode(\App\Category::all());
});
Route::get('api/users', function () {
    return json_encode(\App\User::all());
});
Route::get('api/groups', function () {
    return json_encode(\App\Group::all());
});

Route::post('groups/{group}/users/{user}/add', 'GroupController@addParticipant');
Route::delete('groups/{group}/users/{user}/remove', 'GroupController@removeParticipant');